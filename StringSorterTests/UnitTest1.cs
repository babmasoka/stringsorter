using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringSorter;
using System;
using System.Linq;

namespace StringSorterTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Given_An_EmptyStingInput_ShouldThrowAnInvalidOperationException()
        {
            //Arrange
            var stringInput = "";
            var adapter = new StringSorterAdapter();
            //Assert
            var ex = Assert.ThrowsException<InvalidOperationException>(()=> adapter.SortInputString(stringInput));
            Assert.AreEqual("Cannot sort an empty string.", ex.Message);
        }

        [TestMethod]
        public void Given_A_ValidInput_ShouldMapToLowerCases()
        {
            //Arrange
            var stringInput = "Dumakude Masoka";
            //Act
            var result = stringInput.MapToLowerCases();
            //Assert
            Assert.IsFalse(result.Any(char.IsUpper));
        }

        [TestMethod]
        public void Given_A_ValidInput_ShouldRemovePunctuationMarks()
        {
            //Arrange
            var stringInput = "Dumakude Masoka!";
            var adapter = new StringSorterAdapter();
            //Act
            var result = stringInput.RemovePunctuationMarks();
            //Assert
            Assert.IsFalse(!result.Any(char.IsLetterOrDigit));
        }

        [TestMethod]
        public void Given_A_ValidInput_ShouldMapStringToLowerCasesAndRemovePunctuationMarks_ThenReturnSortedString()
        {
            //Arrange
            var stringInput = "Duma!";
            var adapter = new StringSorterAdapter();
            //Act
            var result = adapter.SortInputString(stringInput);
            //Assert
            Assert.AreEqual("admu", result);
        }

        [TestMethod]
        public void Given_A_ValidInput_ShouldMapStringToLowerCasesAndRemovePunctuationMarks_ThenReturnSortedString2()
        {
            //Arrange
            var stringInput = "Contrary to popular belief, the pink unicorn flies east.";
            var adapter = new StringSorterAdapter();
            //Act
            var result = adapter.SortInputString(stringInput);
            //Assert
            Assert.AreEqual("aaabcceeeeeffhiiiiklllnnnnooooppprrrrssttttuuy", result);
        }
    }
}
