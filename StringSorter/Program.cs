﻿using System;

namespace StringSorter
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var adapter = new StringSorterAdapter();
                Console.WriteLine(adapter.SortInputString(Console.ReadLine()));
            }
        }
    }
}
