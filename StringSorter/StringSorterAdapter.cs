﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace StringSorter
{
    public class StringSorterAdapter
    {
        public string SortInputString(string stringInput)
        {
            if (string.IsNullOrEmpty(stringInput))
            {
                throw new InvalidOperationException("Cannot sort an empty string.");
            }
            string resultString = stringInput.MapToLowerCases();
            resultString = resultString.RemovePunctuationMarks();
            return String.Concat(resultString.OrderBy(c => c));
        }        
    }
}
