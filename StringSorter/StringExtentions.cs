﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace StringSorter
{
    public static class StringExtentions
    {
        public  static string RemovePunctuationMarks(this string resultString)
        {
            Regex reg = new Regex("['\",.!?]");
            resultString = reg.Replace(resultString, string.Empty).Replace(" ", string.Empty);
            return resultString;
        }

        public static string MapToLowerCases(this string stringInput)
        {
            if (stringInput.Any(char.IsUpper))
            {
                return stringInput.ToLower();
            }
            return stringInput;
        }
    }
}
